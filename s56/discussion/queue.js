let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
}

function dequeue() {
    // In here you are going to remove the last element in the array
}

function front() {
    // In here, you are going to remove the first element
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
}

function isEmpty() {
    //it will check whether the function is empty or not
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};