// console.log("Hello");


// ----------------------------SECTION---------------------------- //
// Document Ojbect Model (DOM)
	// Allows us to access or modify the properties of an html element in a webpage
	//  It is standard on how to get, change, add or delete HTML elements
	// We will be focusing only with DOM in terms of managing forms

	// Using the query selector, it can access the HTML elements using 

	// CSS selectors to target specific element
		// id selector (#)
		// class selector (.)
		// tag/type selector (html tags)
		// attribute selector ([attribute])

	// Query selectors has two types: querySelector and querySelectorAll

// let firstElement = document.querySelector('#txt-first-name');
// console.log(firstElement);
	
	// querySelector
	let secondElemeent = document.querySelector('.full-name');
	console.log(secondElemeent)

	// querySelectorAll
	let thirdElement = document.querySelectorAll('.full-name');
	console.log(thirdElement)

	// getElements
	let element = document.getElementById('#fullName');
	console.log(element);

	element = document.getElementsByClassName('full-name');
	console.log(element);

	

// ----------------------------SECTION---------------------------- //
// EVENT LISTENERS
	// Whenever a user interacts with a webpage, this action is considered as event
	// Working with events is large part of  creating interactivity in a webpage
	// Specific function will be invoked if the event happen


// COMMON EVENTS
// change
// click
// load
// keydown
// keyup

// function addEventListener
	// 2 arguments
	// 1st argument: string identifying the event
	// 2nd argument: function that the listener will invoke once the specified event occur.


	let fullName = document.querySelector('#fullName');
	console.log(fullName.innerHTML);

	let firstElement = document.querySelector('#txt-first-name');
	console.log(firstElement);

	let txtLastName = document.querySelector('#txt-last-name');
	console.log(txtLastName);

	firstElement.addEventListener('keydown', ()=> {
		console.log(firstElement.value)
		// let valueInput = firstElement.value;

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
	});

	// firstElement.addEventListener('change', () => {
	// 	console.log(firstElement.value)
	// })
	
	

	txtLastName.addEventListener('keyup', ()=> {
		console.log(txtLastName.value)

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
	});


	// --------------- ACTIVITY ---------------


	let textColor = document.querySelector('#text-color')
	let fullNameColor = document.getElementById("fullName")

	textColor.addEventListener('change', ()=> {
		fullNameColor.style.color = textColor.value
	})

	

	
	
	

