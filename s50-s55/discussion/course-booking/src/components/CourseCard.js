import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function CourseCard(props){
    // consume the content of the UserContext.
    const {user} = useContext(UserContext);
    
    console.log(props.courseProp)
     
    
    // object destructuring
    const {_id, name, description, price} = props.courseProp;
    console.log(_id)

    // Syntax: const [getter, setter] = useState(initilizer)

    const [seat, setSeat] = useState(30);
    const [count, setCount] = useState(0);
   
    // const[isDisabled, setIsDisabled] = useState(false);

    function enroll(){
        if(count !== 30){
            setCount(count + 1);
            setSeat(count - 1);

            Swal2.fire({
                title : 'Thank you for enrolling!',
                icon : 'info',
                text : 'Proceed to your profile'
            })
        
        }else{
            // setIsDisabled(true);
            // alert('No more seats');
        }
    }

    // The function or the side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is/are changes on our dependencies.

    useEffect(()=>{
        if (count === 30){
            setIsDisabled(true);
            // Swal2.fire({
            //     title : 'Course Full',
            //     icon : 'info',
            //     text : 'You got the last seat'
            // })
        } 
        
    }, [count]);

    return (

        <Container className='my-3'>
            <Row>
                <Col className='col-12 col-md-12'>
                <Card>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>{description}</Card.Subtitle>
                        <Card.Text>This is a sample course offering.</Card.Text>
                           
                        <Card.Title>Price</Card.Title>
                        <Card.Subtitle>Php {price}</Card.Subtitle>

                        <Card.Subtitle>Enrollees:</Card.Subtitle>
                        <Card.Text>No. of enrollees: {count}</Card.Text>

                        {
                            user !== null
                            ?
                            <Button className='mt-4' as = {Link} to = {`/courses/${_id}`}>Details</Button>
                            :
                            <Button className='mt-4' as = {Link} to = '/login'>Login to Enroll</Button>

                        }
                      
                    </Card.Body>
                </Card>
                </Col>
            </Row>
        </Container>
    )
}